import numpy as np
import cv2
from PIL import Image
import glob,os
import matplotlib.pyplot as plt
import pandas as pd
import keras
import csv
from keras.datasets import mnist
from keras.models import Sequential, Model, load_model
from keras.layers import Dense, Dropout, Flatten, BatchNormalization
from keras.layers import Conv2D, MaxPooling2D, Input, UpSampling2D
from keras import backend as K
from keras import regularizers
from keras.utils import plot_model
from sklearn.cluster import KMeans
from sklearn.metrics import silhouette_score
import scipy.misc

#---------------------------------TUNABLE PARAMETERS-----------------------------------------#

training_enabled = False
# boolean: ~if set to 'True', enables training of the autoencoder with dataset located in 
#           img_train_path
#          ~if set to 'False', autoencoder pre-trained weigths from autoencoder_model.h5 file are loaded 
img_train_path = ['/home/jorge/Documents/Code/bm_test/ObjectMatchingChallenge/Bounding_Box_Cutouts/', '/home/jorge/Documents/Code/bm_test/ObjectMatchingChallenge/Bounding_Box_Cutouts (2)/'] 
# string or list of strings: path where the training set is stored (only valid if training_enabled
#                            is set to 'True')
img_test_path = '/home/jorge/Documents/Code/bm_test/ObjectMatchingChallenge/test0_cam1/'
# string: path where the test images are stored
num_people = None
# integer: number of people present in the test image dataset. If set to None, the code will use the silhouette method as a proxy to estimate most likely number of classes (people)
#--------------------------------------------------------------------------------------------#


#--------------------------Data loading and formatting step----------------------------------#

# target size to format all train and test images
t_width = 160
t_height = 240

#Load and resize training images if required, stores result in imgs_train
if training_enabled:
    num_images_train = min([len(glob.glob(path +'*.png')) for path in img_train_path])*len(img_train_path)
    imgs_train = np.zeros([num_images_train,t_height,t_width,3])
    i=0
    print("Loading and formatting " + str(num_images_train) + " training images...")
    for c, path in enumerate(img_train_path):
        for filename in sorted(glob.glob(path +'*.png')): 
            im=Image.open(filename)
            width, height = im.size
            if height > 1.5*width:
                n_height = 1.5*width
                im = im.crop((0,0,width,n_height))
            if width > t_width:
                im = im.resize((t_width, t_height), Image.ANTIALIAS)
            else:
                im = im.resize((t_width, t_height), Image.NEAREST) 
            imgs_train[i,:,:,:] = im
            i = i+1
            if i == (c+1)*(num_images_train/len(img_train_path)):
                break
    imgs_train = imgs_train.astype('float32')/255

#Load and resize test images, store result in imgs_test
num_images_test = len(glob.glob(img_test_path +'*.png'))
imgs_test = np.zeros([num_images_test,t_height,t_width,3])
i=0
print("Loading and formatting " + str(num_images_test) + " test images...")
for filename in sorted(glob.glob(img_test_path +'*.png')): 
    im=Image.open(filename)
    width, height = im.size
    if height > 1.5*width:
        n_height = 1.5*width
        im = im.crop((0,0,width,n_height))
    if width > t_width:
        im = im.resize((t_width, t_height), Image.ANTIALIAS)
    else:
        im = im.resize((t_width, t_height), Image.NEAREST) 
    imgs_test[i,:,:,:] = im
    
    i = i+1    
    
imgs_test = imgs_test.astype('float32')/255




#------------------------------Human segmentation step-----------------------------#

#Reusing people segmentation code and associated config files from Adrian Rosebrock, source: https://www.pyimagesearch.com/2018/11/19/mask-r-cnn-with-opencv/ 

rcnn_weightsPath = "frozen_inference_graph.pb"
rcnn_configPath = "mask_rcnn_inception_v2_coco_2018_01_28.pbtxt"
rcnn_labelsPath = "object_detection_classes_coco.txt"
LABELS = open(rcnn_labelsPath).read().strip().split("\n")


net = cv2.dnn.readNetFromTensorflow(rcnn_weightsPath, rcnn_configPath)

if training_enabled:
    imgs_train = (imgs_train*255).astype("uint8")
    segmented_imgs_train = np.zeros(imgs_train.shape).astype("uint8")
    print("Segmenting " + str(num_images_train) + " training images...")
    for k in range(num_images_train):
        blob = cv2.dnn.blobFromImage(imgs_train[k,:,:,:], swapRB=True, crop=False)
        net.setInput(blob)
        try:
            (boxes, masks) = net.forward(["detection_out_final", "detection_masks"])
        except:
            segmented_imgs_train[k,:,:,:] = imgs_train[k,:,:,:]
            continue    
        for i in range(0, boxes.shape[2]):
            # extract the class ID of the detection along with the confidence
            # (i.e., probability) associated with the prediction
            classID = int(boxes[0, 0, i, 1])
            confidence = boxes[0, 0, i, 2]
 
            # filter out weak predictions by ensuring the detected probability
            # is greater than the minimum probability
            if (confidence > 0.5) and (LABELS[classID]=='person'):
                clone = imgs_train[k,:,:,:].copy()
                # scale the bounding box coordinates back relative to the
                # size of the image and then compute the width and the height
                # of the bounding box
                box = boxes[0, 0, i, 3:7] * np.array([t_width, t_height,t_width, t_height])
                (startX, startY, endX, endY) = box.astype("int")
                boxW = endX - startX
                boxH = endY - startY

                # extract the pixel-wise segmentation for the object, resize
                # the mask such that it's the same dimensions of the bounding
                # box, and then finally threshold to create a *binary* mask
                mask = masks[i, classID]
                mask = cv2.resize(mask, (boxW, boxH), interpolation=cv2.INTER_NEAREST)
                mask = (mask > 0.3)
 
                # extract the ROI of the image
                roi = clone[startY:endY, startX:endX]
           
                # convert the mask from a boolean to an integer mask with
                # to values: 0 or 255, then apply the mask
                visMask = (mask*255).astype("uint8")
                segmented_imgs_train[k,startY:endY, startX:endX,:] = cv2.bitwise_and(roi, roi, mask=visMask)
                break
            
        if np.count_nonzero(segmented_imgs_train[k,:,:,:]) == 0:
            segmented_imgs_train[k,:,:,:] = imgs_train[k,:,:,:]
        if (k%100 == 0) and (k>0):
            print("Segmented " + str(k) + " of " str(num_images_train) + " training images")
    print('Done with training set segmentation!')
    segmented_imgs_train = segmented_imgs_train.astype('float32')/255


imgs_test = (imgs_test*255).astype("uint8")
segmented_imgs_test = np.zeros(imgs_test.shape).astype("uint8")
print("Segmenting " + str(num_images_test) + " test images...")
for k in range(num_images_test):
    blob = cv2.dnn.blobFromImage(imgs_test[k,:,:,:], swapRB=True, crop=False)
    net.setInput(blob)
    try:
        (boxes, masks) = net.forward(["detection_out_final", "detection_masks"])
    except:
        segmented_imgs_test[k,:,:,:] = imgs_test[k,:,:,:]
        continue
    for i in range(0, boxes.shape[2]):
        # extract the class ID of the detection along with the confidence
        # (i.e., probability) associated with the prediction
        classID = int(boxes[0, 0, i, 1])
        confidence = boxes[0, 0, i, 2]
 
        # filter out weak predictions by ensuring the detected probability
        # is greater than the minimum probability
        if (confidence > 0.5) and (LABELS[classID]=='person'):
            clone = imgs_test[k,:,:,:].copy()
            # scale the bounding box coordinates back relative to the
            # size of the image and then compute the width and the height
            # of the bounding box
            box = boxes[0, 0, i, 3:7] * np.array([t_width, t_height,t_width, t_height])
            (startX, startY, endX, endY) = box.astype("int")
            boxW = endX - startX
            boxH = endY - startY

            # extract the pixel-wise segmentation for the object, resize
            # the mask such that it's the same dimensions of the bounding
            # box, and then finally threshold to create a *binary* mask
            mask = masks[i, classID]
            mask = cv2.resize(mask, (boxW, boxH), interpolation=cv2.INTER_NEAREST)
            mask = (mask > 0.3)
 
            # extract the ROI of the image
            roi = clone[startY:endY, startX:endX]
           
            # convert the mask from a boolean to an integer mask with
            # to values: 0 or 255, then apply the mask
            visMask = (mask*255).astype("uint8")
            segmented_imgs_test[k,startY:endY, startX:endX,:] = cv2.bitwise_and(roi, roi, mask=visMask)
            break
            
    if np.count_nonzero(segmented_imgs_test[k,:,:,:]) == 0:
        segmented_imgs_test[k,:,:,:] = imgs_test[k,:,:,:]
    if (k%100 == 0) and (k>0):
        print("Segmented " + str(k) + " of " str(num_images_test) + " test images")
print('Done with test set segmentation!')        
segmented_imgs_test = segmented_imgs_test.astype('float32')/255

#--------------------Autoencodel model training/inference step---------------------#

#get model parameters either via training or loading weights
if training_enabled:
    #batch size and epochs can be tuned depending on size of training dataset
    batch_size = 50
    epochs = 20
    
    input_img = Input(shape=(t_height,t_width,3))
    x = Conv2D(16,(3,3), activation='relu', padding='same')(input_img)
    x = MaxPooling2D((2,2), padding='same')(x)
    x = Conv2D(8,(3,3), activation='relu', padding='same')(x)
    x = MaxPooling2D((2,2), padding='same')(x)
    x = Conv2D(8,(3,3), activation='relu', padding='same')(x)
    encoded = MaxPooling2D((2,2), padding='same', name='encoder')(x)

    x = Conv2D(8, (3, 3), activation='relu', padding='same')(encoded)
    x = UpSampling2D((2, 2))(x)
    x = Conv2D(8, (3, 3), activation='relu', padding='same')(x)
    x = UpSampling2D((2, 2))(x)
    x = Conv2D(16, (3, 3), activation='relu', padding='same')(x)
    x = UpSampling2D((2, 2))(x)
    decoded = Conv2D(3, (3, 3), activation='sigmoid', padding='same')(x)

    autoencoder = Model(input_img, decoded)
    autoencoder.compile(loss=keras.losses.categorical_crossentropy,
              optimizer=keras.optimizers.sgd(),
              metrics=['accuracy'])

    autoencoder.fit(segmented_imgs_train, segmented_imgs_train, batch_size=batch_size, epochs=epochs, verbose=1,shuffle=False, validation_split=0.2)
    autoencoder.save("autoencoder_model.h5")
    print("Saved model to disk")
else:
    autoencoder = load_model("autoencoder_model.h5")
#running encoder model on test set to get feature representation
encoder = Model(inputs=autoencoder.input, outputs=autoencoder.get_layer('encoder').output)
features_space = encoder.predict(segmented_imgs_test)

#-------------------------------Clustering step---------------------------------#
num_feats = features_space.shape[1]*features_space.shape[2]*features_space.shape[3]
vector_feat = features_space.reshape(num_images_test, num_feats)


if num_people is None:
    print("Finding most likely amount of clusters (people)")
    #calculating histogram of each image to augment feature representation in order to get an estimate of the number of clusters (a.k.a. people)
    vector_feat_hist = np.zeros([num_images_test, num_feats + 256*3])
    vector_feat_hist[:,:num_feats] = vector_feat
    b = np.arange(0.5,256.5,1)/255

    for i in range(num_images_test):
        h=np.zeros(256*3)
        for channel in range(3):
            h[channel*256:(channel+1)*256] = np.histogram(segmented_imgs_test[i,:,:,channel], 256)[0]   
        vector_feat_hist[i,-256*3:] = h
    #using silhouette score to estimate most possible number of clusters
    K = range(2,10)
    Sum_of_squared_distances = []
    silhouette = []
    for k in K:
        km = KMeans(n_clusters=k)
        km = km.fit(vector_feat_hist)
        
        silhouette.append(silhouette_score(vector_feat, km.labels_, metric='euclidean'))   

    opt_k =np.where(silhouette == np.amax(silhouette))[0] + 2
    num_people = opt_k[0]
#running k-means with estimated (or provided) number of clusters
result = KMeans(n_clusters=num_people, random_state=0).fit(vector_feat)
print("Clustered images into "+str(num_people) + " groups of people. Saving results to csv file...")

#generating csv file to store resulting labels
with open('person_matches.csv', 'wb') as csvfile:
    filewriter = csv.writer(csvfile, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)
    i=0
    for filename in sorted(glob.glob(img_test_path +'*.png')):
        img_name = filename[-9:]
        filewriter.writerow([img_name, str(result.labels_[i])])
        i = i+1

print("Done!")
