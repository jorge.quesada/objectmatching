# ObjectMatching

This is a simple python code for unsupervised people matching in an image dataset. It is assumed that the dataset is composed of cropped images of different people, and that some of these people appear multiple times across the dataset. The algorithm consists simply in segmenting the people from the images, then encoding the information from the segmented images using a small autoencoder network (either trained on the provided dataset or using the pre-trained weights) to finally cluster this representation (augmented with the histogram of each image to increase the weight of the color components) to get groups of images that contain the same person.

DISCLAIMER: This was a weekend school project, so it is not intended to (and does not) provide state-of-the-art performance in an object matching task, but namely to serve as a starting point or proof of concept of the representational power of CNN encoders. There is plenty of room for improvement, such as using a more powerful autoencoder network such as U-net, or including information from a face detector into the representation. 

# Requirements
The code utilizes modules from OpenCV, Keras, matplotlib, pandas, sklearn and scipy. These modules need to be properly installed before running the code. This code also requires the configuration file **frozen_inference_graph.pb** to be on the same directory as the objectmatch.py script. You can either download it from the original website [https://www.pyimagesearch.com/2018/11/19/mask-r-cnn-with-opencv/](https://www.pyimagesearch.com/2018/11/19/mask-r-cnn-with-opencv/)  or from my drive [https://drive.google.com/file/d/1N3v41Lha1f9X59oCjh3S6TnKPnyuUfqH/view?usp=sharing](https://drive.google.com/file/d/1N3v41Lha1f9X59oCjh3S6TnKPnyuUfqH/view?usp=sharing)

# Usage
The code can be run by executing the objectmatch.py script in a terminal. There are several parameters that must be set before running it:

**training_enabled** = boolean

        -if set to 'True', enables training of the autoencoder with dataset located in img_train_path
        
        -if set to 'False', autoencoder pre-trained weigths from autoencoder_model.h5 file are loaded 
        
**img_train_path** = string or list of strings

        -path where the training set is stored (only valid if training_enabled is set to 'True')
        
**img_test_path** = string

        -path where the test images are stored
        
**num_people** = integer

        -number of people present in the test image dataset. If set to None (default), the code will use the silhouette method as a proxy to estimate most likely number of classes (people)